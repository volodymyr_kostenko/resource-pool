package com.test.resourcepool;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.locks.Lock;

@Getter
@AllArgsConstructor
public class ResourceMetadata {

    private ResourceState state;
    private Lock lock;
}
