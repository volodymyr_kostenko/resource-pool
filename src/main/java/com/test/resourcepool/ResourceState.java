package com.test.resourcepool;

public enum ResourceState {
    READY,
    IN_USE,
    FINALIZED
}
