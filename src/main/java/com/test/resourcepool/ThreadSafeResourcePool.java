package com.test.resourcepool;

import com.test.resourcepool.exception.ResourcePoolCanNotBeOpenedTwiceException;
import com.test.resourcepool.exception.ResourcePoolIsNotOpenedException;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.test.resourcepool.PoolState.*;
import static com.test.resourcepool.ResourceState.*;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class ThreadSafeResourcePool<R> implements ResourcePool<R> {

    private final Map<R, ResourceMetadata> resources = new ConcurrentHashMap<>();
    private final AtomicInteger resourcesCount = new AtomicInteger();
    private final AtomicReference<PoolState> state = new AtomicReference<>(NOT_OPENED);
    private final static long SLEEP_TIME = 1000L;

    @Override
    public void open() {
        if (!state.compareAndSet(NOT_OPENED, OPENED)) {
            throw new ResourcePoolCanNotBeOpenedTwiceException();
        }
    }

    @Override
    public boolean isOpen() {
        return state.compareAndSet(OPENED, OPENED);
    }

    private boolean isClosed() {
        return state.compareAndSet(CLOSED, CLOSED);
    }

    private boolean isClosing() {
        return state.compareAndSet(CLOSING, CLOSING);
    }

    private void assertOpen() {
        if (!isOpen()) {
            throw new ResourcePoolIsNotOpenedException();
        }
    }

    @Override
    public R acquire() throws InterruptedException {
        while (true) {
            assertOpen();

            R resource = tryToAcquire();
            if (resource != null) {
                return resource;
            }

            Thread.sleep(SLEEP_TIME);
        }
    }

    private R tryToAcquire() {
        Optional<Map.Entry<R, ResourceMetadata>> resourceToMetadata = resources.entrySet().stream()
                .filter(resourceToState -> READY.equals(resourceToState.getValue().getState()))
                .findFirst();

        if (!resourceToMetadata.isPresent()) {
            return null;
        }

        Lock lock = resourceToMetadata.get().getValue().getLock();
        ResourceState resourceState = resourceToMetadata.get().getValue().getState();
        boolean lockAcquired = lock.tryLock();
        if (!lockAcquired) {
            return null;
        }

        try {
            if (READY.equals(resourceState)) {
                R resource = resourceToMetadata.get().getKey();
                resources.put(resource, new ResourceMetadata(IN_USE, lock));
                return resource;
            }
        } finally {
            lock.unlock();
        }

        return null;
    }

    @Override
    public R acquire(long timeout, TimeUnit unit) throws InterruptedException {
        long absoluteTimeout = System.currentTimeMillis() + unit.toMillis(timeout);

        while (true) {
            assertOpen();

            R resource = tryToAcquire();
            if (resource != null) {
                return resource;
            }

            if (absoluteTimeout < System.currentTimeMillis()) {
                return null;
            }

            Thread.sleep(SLEEP_TIME);
        }
    }

    @Override
    public void release(R resource) {
        if (isClosed()) {
            return;
        }

        ResourceMetadata metadata = resources.get(resource);
        if (metadata == null) {
            return;
        }
        Lock lock = metadata.getLock();
        try {
            lock.lock();

            ResourceState resourceState = metadata.getState();
            if (READY.equals(resourceState)
                    || FINALIZED.equals(resourceState)) {
                return;
            }

            resources.put(resource, new ResourceMetadata(READY, lock));

        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean add(R resource) {
        if (isClosing() || isClosed()) {
            return false;
        }

        if (!resources.containsKey(resource)) {
            resources.put(resource, new ResourceMetadata(READY, new ReentrantLock()));
            resourcesCount.incrementAndGet();
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(R resource) throws InterruptedException {
        if (isClosed()) {
            return false;
        }

        ResourceMetadata metadata = resources.get(resource);
        if (metadata == null) {
            return false;
        }
        while (true) {
            Lock lock = metadata.getLock();
            boolean lockAcquired = lock.tryLock(SLEEP_TIME, MILLISECONDS);

            if (!lockAcquired) {
                continue;
            }

            try {
                ResourceState resourceState = metadata.getState();

                if (READY.equals(resourceState)
                        || FINALIZED.equals(resourceState)) {
                    resources.remove(resource);
                    resourcesCount.decrementAndGet();
                    return true;
                }

            } finally {
                lock.unlock();
            }

        }
    }

    @Override
    public boolean removeNow(R resource) {
        if (isClosed()) {
            return false;
        }

        ResourceMetadata metadata = resources.get(resource);
        if (metadata == null) {
            return false;
        }

        if (resources.remove(resource) != null) {
            resourcesCount.decrementAndGet();
            return true;
        }

        return false;
    }

    @Override
    public void close() throws InterruptedException {
        if (state.compareAndSet(NOT_OPENED, CLOSED)) {
            return;
        }
        if (isClosing()) {
            return;
        }
        if (isClosed()) {
            return;
        }
        if (!state.compareAndSet(OPENED, CLOSING)) {
            return;
        }

        int finalized = 0;
        while (true) {
            if (isClosed()
                    || resourcesCount.compareAndSet(finalized, -1)) {
                return;
            }

            Optional<Map.Entry<R, ResourceMetadata>> resourceToMetadata = getFirstReady();
            if (!resourceToMetadata.isPresent()) {
                continue;
            }

            Lock lock = resourceToMetadata.get().getValue().getLock();
            ResourceState resourceState = resourceToMetadata.get().getValue().getState();
            boolean lockAcquired = lock.tryLock(SLEEP_TIME, MILLISECONDS);
            if (!lockAcquired) {
                continue;
            }

            try {
                if (READY.equals(resourceState)) {
                    resources.put(resourceToMetadata.get().getKey(), new ResourceMetadata(FINALIZED, lock));
                    finalized++;
                }
            } finally {
                lock.unlock();
            }

        }
    }

    private Optional<Map.Entry<R, ResourceMetadata>> getFirstReady() {
        return resources.entrySet().stream()
                .filter(resourceToState -> READY.equals(resourceToState.getValue().getState()))
                .findFirst();
    }

    @Override
    public void closeNow() {
        state.getAndSet(CLOSED);
    }


//    private final Map<R, ResourceMetadata> resources = new ConcurrentHashMap<>();
//    private final AtomicInteger resourcesCount = new AtomicInteger();
//    private final AtomicReference<PoolState> state = new AtomicReference<>(NOT_OPENED);
//    private final static long SLEEP_TIME = 1000L;
//
//    @Override
//    public void open() {
//        if (!state.compareAndSet(NOT_OPENED, OPENED)) {
//            throw new ResourcePoolCanNotBeOpenedTwiceException();
//        }
//    }
//
//    @Override
//    public boolean isOpen() {
//        return state.compareAndSet(OPENED, OPENED);
//    }
//
//    private boolean isClosed() {
//        return state.compareAndSet(CLOSED, CLOSED);
//    }
//
//    private void assertOpen() {
//        if (isOpen()) {
//            throw new ResourcePoolIsNotOpenedException();
//        }
//    }
//
//    @Override
//    public R acquire() throws InterruptedException {
//        while (true) {
//            assertOpen();
//
//            Optional<Map.Entry<R, ResourceMetadata>> resourceToMetadata = resources.entrySet().stream()
//                    .filter(resourceToState -> READY.equals(resourceToState.getValue().getState()))
//                    .findFirst();
//
//            if (!resourceToMetadata.isPresent()) {
//                continue;
//            }
//
//            ResourceMetadata metadata = resourceToMetadata.get().getValue();
//            synchronized (metadata.getLock()) {
//                if (READY.equals(metadata.getState())) {
//                    R resource = resourceToMetadata.get().getKey();
//                    resources.put(resource, new ResourceMetadata(IN_USE, metadata.getLock()));
//                    return resource;
//                }
//            }
//
//            Thread.sleep(SLEEP_TIME);
//        }
//    }
//
//    @Override
//    public R acquire(long timeout, TimeUnit unit) throws InterruptedException {
//        long deadline = System.currentTimeMillis() + unit.toMillis(timeout);
//
//        while (true) {
//            assertOpen();
//
//            Optional<Map.Entry<R, ResourceMetadata>> resourceToMetadata = resources.entrySet().stream()
//                    .filter(resourceToState -> READY.equals(resourceToState.getValue().getState()))
//                    .findFirst();
//
//            if (!resourceToMetadata.isPresent()) {
//                continue;
//            }
//
//            ResourceMetadata metadata = resourceToMetadata.get().getValue();
//            synchronized (metadata.getLock()) {
//                if (READY.equals(metadata.getState())) {
//                    R resource = resourceToMetadata.get().getKey();
//                    resources.put(resource, new ResourceMetadata(IN_USE, metadata.getLock()));
//                    return resource;
//                }
//            }
//
//            if (deadline < System.currentTimeMillis()) {
//                return null;
//            }
//
//            Thread.sleep(SLEEP_TIME);
//        }
//    }
//
//    @Override
//    public void release(R resource) {
//        if (isClosed()) {
//            return;
//        }
//
//        ResourceMetadata metadata = resources.get(resource);
//        if (metadata == null) {
//            return;
//        }
//        synchronized (metadata.getLock()) {
//            ResourceState resourceState = metadata.getState();
//            if (FINALIZED.equals(resourceState)
//                    || READY.equals(resource)) {
//                return;
//            }
//
//            resources.put(resource, new ResourceMetadata(READY, metadata.getLock()));
//        }
//    }
//
//    @Override
//    public boolean add(R resource) {
//        if (isClosed()) {
//            return false;
//        }
//
//        if (!resources.containsKey(resource)) {
//            resources.put(resource, new ResourceMetadata(READY, new Object()));
//            resourcesCount.incrementAndGet();
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public boolean remove(R resource) throws InterruptedException {
//        if (isClosed()) {
//            return false;
//        }
//
//        ResourceMetadata metadata = resources.get(resource);
//        if (metadata == null) {
//            return false;
//        }
//        while (true) {
//            synchronized (metadata.getLock()) {
//                ResourceState resourceState = metadata.getState();
//                if (FINALIZED.equals(resourceState)) {
//                    return false;
//                }
//
//                if (READY.equals(resourceState)) {
//                    resources.remove(resource);
//                    resourcesCount.decrementAndGet();
//                    return true;
//                }
//            }
//
//            Thread.sleep(SLEEP_TIME);
//        }
//    }
//
//    @Override
//    public boolean removeNow(R resource) {
//        if (isClosed()) {
//            return false;
//        }
//
//        ResourceMetadata metadata = resources.get(resource);
//        if (metadata == null) {
//            return false;
//        }
//        synchronized (metadata.getLock()) {
//            if (FINALIZED.equals(metadata.getState())) {
//                return false;
//            }
//
//            resources.remove(resource);
//            resourcesCount.decrementAndGet();
//            return true;
//        }
//    }
//
//    @Override
//    public void close() throws InterruptedException {
//        //TODO add proper exception handling
//        if (state.compareAndSet(CLOSING, CLOSING)) {
//            return;
//        }
//        if (isClosed()) {
//            return;
//        }
//        if (!state.compareAndSet(OPENED, CLOSING)) {
//            return;
//        }
//
//        int finalized = 0;
//        while (resourcesCount.compareAndSet(finalized, -1)) {
//            if (isClosed()) {
//                return;
//            }
//
//            Optional<Map.Entry<R, ResourceMetadata>> resourceToMetadata = resources.entrySet().stream()
//                    .filter(resourceToState -> READY.equals(resourceToState.getValue().getState()))
//                    .findFirst();
//
//            if (!resourceToMetadata.isPresent()) {
//                continue;
//            }
//
//            ResourceMetadata metadata = resourceToMetadata.get().getValue();
//            synchronized (metadata.getLock()) {
//                if (READY.equals(metadata.getState())) {
//                    resources.put(resourceToMetadata.get().getKey(), new ResourceMetadata(FINALIZED, metadata.getLock()));
//                    finalized++;
//                }
//            }
//
//            Thread.sleep(SLEEP_TIME);
//        }
//    }
//
//    @Override
//    public void closeNow() {
//        state.getAndSet(CLOSED);
//    }
}
