package com.test.resourcepool.exception;

public class ResourcePoolException extends RuntimeException {

    public ResourcePoolException(String message) {
        super(message);
    }
}
