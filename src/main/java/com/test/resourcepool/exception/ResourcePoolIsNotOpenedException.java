package com.test.resourcepool.exception;

public class ResourcePoolIsNotOpenedException extends ResourcePoolException {

    public ResourcePoolIsNotOpenedException() {
        super("Resource pool is not opened");
    }
}
