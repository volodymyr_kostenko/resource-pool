package com.test.resourcepool.exception;

public class ResourcePoolCanNotBeOpenedTwiceException extends ResourcePoolException {

    public ResourcePoolCanNotBeOpenedTwiceException() {
        super("Resource pool was already opened or it is already closing/closed");
    }
}
