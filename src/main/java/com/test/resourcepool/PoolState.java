package com.test.resourcepool;

enum PoolState {
    NOT_OPENED,
    OPENED,
    CLOSING,
    CLOSED
}
