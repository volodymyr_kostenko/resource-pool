package com.test.resourcepool;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public interface ResourcePool<R> extends AutoCloseable {

    void open();

    boolean isOpen();

    R acquire() throws InterruptedException;

    R acquire(long timeout, TimeUnit timeUnit) throws InterruptedException;

    void release(R resource);

    boolean add(R resource);

    boolean remove(R resource) throws InterruptedException;

    boolean removeNow(R resource);

    void closeNow();
}
