package com.test.resourcepool;

import com.test.resourcepool.exception.ResourcePoolIsNotOpenedException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ThreadSafeResourcePoolTest {

    private ResourcePool<String> resourcePool = new ThreadSafeResourcePool<>();

    private ExecutorService executor = Executors.newFixedThreadPool(3);
    private final String resource1 = "resource1";
    private final String resource2 = "resource2";
    private final long timeout = 1000L;

    @AfterEach
    public void init() {
        executor.shutdown();
    }

    @Test
    public void shouldNotAcquireResourceWhenPoolIsNotOpened() {
        //expected
        assertThatThrownBy(() -> resourcePool.acquire())
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }

    @Test
    public void shouldAddResourceInNotOpenedState() {
        //when
        boolean added = resourcePool.add(resource1);

        //then
        assertThat(added).isTrue();
    }

    @Test
    public void shouldAddResourceInOpenedState() {
        //given
        resourcePool.open();

        //when
        boolean added = resourcePool.add(resource1);

        //then
        assertThat(added).isTrue();
    }

    @Test
    public void shouldNotAddResourceWhenInClosedState() {
        //given
        resourcePool.closeNow();

        //expected
        assertThat(resourcePool.add(resource2)).isFalse();
    }

//    @Test
//    public void shouldNotAddWhenInClosingState() throws Exception {
//        //given
//        resourcePool.open();
//        resourcePool.add(resource1);
//        String resource = resourcePool.acquire();
//
//        //expected
//        executor.submit(() -> {
//            assertThat(resourcePool.add(resource2)).isFalse();
//            resourcePool.release(resource);
//        });
//
//        //when
//        resourcePool.close();
//    }

    @Test
    public void shouldAcquireResourceAfterRelease() throws InterruptedException {
        //given
        resourcePool.open();
        resourcePool.add(resource1);
        String resource = resourcePool.acquire();

        //when
        resourcePool.release(resource);

        //then
        assertThat(resourcePool.acquire()).isEqualTo(resource1);
    }

    @Test
    public void shouldNotAcquireResourceWhenNotOpened() {
        resourcePool.add(resource1);

        //expected
        assertThatThrownBy(() -> resourcePool.acquire())
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }

    @Test
    public void shouldNotAcquireResourceWhenClosed() {
        resourcePool.add(resource1);
        resourcePool.open();
        resourcePool.closeNow();

        //expected
        assertThatThrownBy(() -> resourcePool.acquire())
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }

    @Test
    public void shouldAcquireResourceWithinTimeoutAfterRelease() throws InterruptedException {
        //given
        resourcePool.open();
        resourcePool.add(resource1);
        String resource = resourcePool.acquire();

        //when
        resourcePool.release(resource);

        //then
        assertThat(resourcePool.acquire(timeout, MILLISECONDS)).isEqualTo(resource1);
    }

    @Test
    public void shouldNotAcquireResourceWithinTimeoutWhenNotOpened() {
        resourcePool.add(resource1);

        //expected
        assertThatThrownBy(() -> resourcePool.acquire(timeout, MILLISECONDS))
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }

    @Test
    public void shouldNotAcquireResourceWithinTimeoutWhenClosed() {
        resourcePool.add(resource1);
        resourcePool.open();
        resourcePool.closeNow();

        //expected
        assertThatThrownBy(() -> resourcePool.acquire(timeout, MILLISECONDS))
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }

    @Test
    public void shouldRemove() throws InterruptedException {
        //given
        resourcePool.add(resource1);

        //expected
        assertThat(resourcePool.remove(resource1)).isTrue();
    }

    @Test
    public void shouldNotRemoveIfResourceIsNotPresent() throws InterruptedException {
        //expected
        assertThat(resourcePool.remove(resource1)).isFalse();
    }

    @Test
    public void shouldNotRemoveWhenInClosedState() throws InterruptedException {
        //given
        resourcePool.add(resource1);
        resourcePool.closeNow();

        //expected
        assertThat(resourcePool.remove(resource1)).isFalse();
    }

    @Test
    public void shouldBlockRemoveUntilResourceIsReleased() throws InterruptedException {
        //given
        resourcePool.add(resource1);
        resourcePool.open();
        String resource = resourcePool.acquire();

        //expected
        executor.submit(() -> assertThat(resourcePool.remove(resource1)).isTrue());

        //when
        resourcePool.release(resource);
    }

    @Test
    public void shouldRemoveNow() {
        //given
        resourcePool.add(resource1);

        //expected
        assertThat(resourcePool.removeNow(resource1)).isTrue();
    }

    @Test
    public void shouldNotRemoveNowIfResourceIsNotPresent() {
        //expected
        assertThat(resourcePool.removeNow(resource1)).isFalse();
    }

    @Test
    public void shouldNotRemoveNowWhenInClosedState() {
        //given
        resourcePool.add(resource1);
        resourcePool.closeNow();

        //expected
        assertThat(resourcePool.removeNow(resource1)).isFalse();
    }

    @Test
    public void shouldNotBlockRemoveNow() throws InterruptedException {
        //given
        resourcePool.add(resource1);
        resourcePool.open();
        String resource = resourcePool.acquire();

        //expected
        assertThat(resourcePool.removeNow(resource)).isTrue();
    }

    @Test
    public void shouldBlockCloseUntilAllResourcesAreReleased() throws InterruptedException {
        //given
        resourcePool.add(resource1);
        resourcePool.add(resource2);
        resourcePool.open();
        String resourceFromPool1 = resourcePool.acquire();
        String resourceFromPool2 = resourcePool.acquire();

        //expected
        executor.submit(() -> {
            try {
                resourcePool.close();
                assertThatThrownBy(() -> resourcePool.acquire())
                        .isInstanceOf(ResourcePoolIsNotOpenedException.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //when
        resourcePool.release(resourceFromPool1);
        resourcePool.release(resourceFromPool2);
    }

    @Test
    public void shouldNotBlockCloseUntilAllResourcesAreReleased() throws InterruptedException {
        //given
        resourcePool.add(resource1);
        resourcePool.add(resource2);
        resourcePool.open();
        resourcePool.acquire();
        resourcePool.acquire();

        //expected
        resourcePool.closeNow();
        assertThatThrownBy(() -> resourcePool.acquire())
                .isInstanceOf(ResourcePoolIsNotOpenedException.class);
    }
}
